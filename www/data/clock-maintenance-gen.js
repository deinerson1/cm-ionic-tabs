{
    'clocks':
    ['{{repeat(3)}}',{
        id: '{{index}}',
        inProduction: 'false',
        inTest: 'false',
        inDevelopment: 'true',
        buildingNumber: '{{numeric(1, 999)}}',
        buildingPicture: 'http://placehold.it/64x64',
        unmBuildingLabel: '{{lorem(1, words)}}',
        vendorBuildingLabel: '{{lorem(1, words)}}',
        lobotimeDeviceId: '{{numeric(100000,999999)}}',
        room: '{{numeric(1,999)}}',
        serialNumber: '{{lorem(1,words)}}',
        ip4Address: '{{numeric(1,255)}}.{{numeric(1,255)}}.{{numeric(1,255)}}.{{numeric(1,255)}}',
        ip4Gateway: '{{numeric(1,255)}}.{{numeric(1,255)}}.{{numeric(1,255)}}.{{numeric(1,255)}}',
        ip4SubnetMask: '{{numeric(1,255)}}.{{numeric(1,255)}}.{{numeric(1,255)}}.{{numeric(1,255)}}',
        ip6Address: '{{numeric(1,255)}}.{{numeric(1,255)}}.{{numeric(1,255)}}.{{numeric(1,255)}}.{{numeric(1,255)}}.{{numeric(1,255)}}',
        ip6Gateway: '{{numeric(1,255)}}.{{numeric(1,255)}}.{{numeric(1,255)}}.{{numeric(1,255)}}.{{numeric(1,255)}}.{{numeric(1,255)}}',
        ip6SubnetMask: '{{numeric(1,255)}}.{{numeric(1,255)}}.{{numeric(1,255)}}.{{numeric(1,255)}}.{{numeric(1,255)}}.{{numeric(1,255)}}',
        macAddress: 'A0:B1:C2:D3:E4:F5',
        notes: '{{lorem(1,paragraphs)}}',
        patchPanelName: '{{lorem(1, words)}}',
        rackName: '{{lorem(1, words)}}',
        unmAssetTag: '{{lorem(1, words)}}',
        maintenancePassword: '{{numeric(100000,999999)}}',
        managerPassword: '{{numeric(100000,999999)}}',
        remotePassword: '{{numeric(100000,999999)}}',
        superUserPassword: '{{numeric(100000,999999)}}',
        clockStatus: function(idx) {
            var choices = ['new', 'network issue', 'New IP address needed', 'NAT IP address', 'installed', 'needs power', 'On Hold - Kronos Ticket', 'On Hold - Help.UNM Ticket', 'spare', 'on order from Kronos', 'purchase request in process', 'return to Kronos', 'device manager (WDM) issue', 'server-side issue', 'needs clock visit by technician', 'soft key issue', ''];
            return choices[this.numeric(0, choices.length - 1)];
        },
        longitude: '{{numeric(-180.000001, 180)}}',
        latitude: '{{numeric(-90.000001, 90)}}',
        created: '{{date(MM/dd/YYYY hh:mm:ss)}}',
        lastUpdated: '{{date(MM/dd/YYYY hh:mm:ss)}}',
        unmTickets: ['{{repeat(2,5)}}',
                     '{{numeric(100000,999999)}}'],
        vendorTickets: ['{{repeat(2,5)}}',
                        '{{numeric(100000,999999)}}']
    }],
    'users':
    ['{{repeat(2)}}',{
        id: '{{index}}',
        netId: 'deinerso',
        password: '{{numeric(100000,999999)}}',
        isActive: 'true',
        picture: 'http://placehold.it/140x140',
        name: 'David Einerson',
        email: 'deinerso@unm.edu',
        registered: '{{date(MM/dd/YYYY hh:mm:ss)}}',
        latitude: '{{numeric(-90.000001, 90)}}',
        longitude: '{{numeric(-180.000001, 180)}}',
        created: '{{date(MM/dd/YYYY hh:mm:ss)}}',
        lastUpdated: '{{date(MM/dd/YYYY hh:mm:ss)}}',
        tags: [
            '{{repeat(5)}}',
            '{{lorem(1)}}'
        ]
    }]
}
