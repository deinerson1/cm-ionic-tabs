// Ionic cm-ionic App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'cm-ionic' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'cm-ionic.services' is found in services.js
// 'cm-ionic.controllers' is found in controllers.js
(function () {
  "use strict";

  angular.module('cm-ionic', [
    'ionic',
    'ngAnimate',
    'ngSanitize',
    'ui.router',
    'firebase',
    'cm-ionic.services',
    'cm-ionic.controllers',
    'cm-ionic.config',
    'cm-ionic.ui-routes',
    'cm-ionic.decorators',
    'cm-ionic.directives',
    'cm-ionic.filters',
    'stateSecurity'

  ])

    .run(['$ionicPlatform', 'simpleLogin', '$log', '$rootScope', function ($ionicPlatform, simpleLogin, $log, $rootScope) {
      $ionicPlatform.ready(function () {
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        if (window.cordova && window.cordova.plugins.Keyboard) {
          cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        }
        if (window.StatusBar) {
          // org.apache.cordova.statusbar required
          StatusBar.styleDefault();
        }

        $log.debug('Run simpleLogin.getUser()');
        simpleLogin.getUser();

        // A "global" logout function accessible from everywhere:
        $rootScope.logout = function () {
          simpleLogin.logout();
        };
      });
    }]);

})();