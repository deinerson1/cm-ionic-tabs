(function() {
  "use strict";

  angular.module('cm-ionic.services', [])

    .factory('ClockService', function(Firebase, $firebase, FBURL) {

      return {
        all : function() {
          var ref = new Firebase(FBURL).child('clocks');
          return $firebase(ref).$asArray();
        },
        get : function(clockId) {
          // @return clockRef Single clock $firebase reference
          var ref = new Firebase(FBURL).child('clocks').child(clockId);
          return $firebase(ref).$asObject();
        }
      };
    })

    .factory('AccountService', function(Firebase, $firebase, FBURL) {

      return {
        all : function() {
          var ref = new Firebase(FBURL).child('users');
          return $firebase(ref).$asArray();
        },
        get : function(userId) {
          var ref = new Firebase(FBURL).child('users').child(userId);
          return $firebase(ref).$asObject();
        }
      };

    });

})();