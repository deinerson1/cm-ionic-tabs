(function () {
  "use strict";
  (function (angular) {
    // Declare app level module which depends on filters, and services
    angular.module('cm-ionic.ui-routes', [])

      .config(function ($stateProvider, $urlRouterProvider) {

        // Ionic uses AngularUI Router which uses the concept of states
        // Learn more here: https://github.com/angular-ui/ui-router
        // Set up the various states which the app can be in.
        // Each state's controller can be found in controllers.js
        $stateProvider

          // setup an abstract state for the tabs directive
          .state('tab', {
            url : "/tab",
            abstract : true,
            templateUrl : "templates/tabs.html",
            data : {
              authRequired : false
            },
            resolve : {
              // forces the page to wait for this promise to resolve before controller is loaded
              // the controller can then inject `user` as a dependency. This could also be done
              // in the controller, but this makes things cleaner (controller doesn't need to worry
              // about auth status or timing of displaying its UI components)
              user : ['simpleLogin', function (simpleLogin) {
                  return simpleLogin.getUser();
                }]
            }
          })

          // the clock tab has its own child nav-view and history
          .state('tab.clock-index', {
            url : '/clocks',
            data : {
              authRequired : true
            },
            views : {
              'clocks-tab' : {
                templateUrl : 'templates/clock-index.html',
                controller : 'ClockIndexCtrl'
              }
            }
          })

          .state('tab.clock-detail', {
            url : '/clock/:clockId',
            data : {
              authRequired : true
            },
            views : {
              'clocks-tab' : {
                templateUrl : 'templates/clock-detail.html',
                controller : 'ClockDetailCtrl'
              }
            }
          })

          .state('tab.add-clock', {
            url : '/add-clock',
            data : {
              authRequired : true
            },
            views : {
              'clocks-tab' : {
                templateUrl : 'templates/add-clock.html'
              }
            }
          })

          .state('tab.account', {
            url : '/account',
            data : {
              authRequired : true
            },
            views : {
              'account-tab' : {
                templateUrl : 'templates/account.html',
                controller : 'AccountCtrl'
              }
            },
            resolve : {
              // forces the page to wait for this promise to resolve before controller is loaded
              // the controller can then inject `user` as a dependency. This could also be done
              // in the controller, but this makes things cleaner (controller doesn't need to worry
              // about auth status or timing of displaying its UI components)
              user : ['simpleLogin', function (simpleLogin) {
                  return simpleLogin.getUser();
                }]
            }

          })

          .state('login', {
            url : '/login',
            data : {
              authRequired : false
            },
            templateUrl : 'templates/login.html',
            controller : 'LoginCtrl',
            resolve : {
              // forces the page to wait for this promise to resolve before controller is loaded
              // the controller can then inject `user` as a dependency. This could also be done
              // in the controller, but this makes things cleaner (controller doesn't need to worry
              // about auth status or timing of displaying its UI components)
              user : ['simpleLogin', function (simpleLogin) {
                  return simpleLogin.getUser();
                }]
            }

          })

          .state('tab.reset-password', {
            url : '/reset-password',
            data : {
              authRequired : false
            },
            views : {
              'account-tab' : {
                templateUrl : 'templates/reset-password.html',
                controller : 'AccountCtrl'
              }
            }
          })

          .state('tab.change-password', {
            url : '/change-password',
            data : {
              authRequired : true
            },
            views : {
              'account-tab' : {
                templateUrl : 'templates/change-password.html',
                controller : 'AccountCtrl'
              }
            }
          })

          .state('tab.about', {
            url : '/about',
            data : {
              authRequired : false
            },
            views : {
              'about-tab' : {
                templateUrl : 'templates/about.html'
              }
            }
          });

        // if none of the above states are matched, use this as the fallback
        $urlRouterProvider.otherwise('/login');

      });

  })(angular);

})();

