/**
 * .config Module injected in app js and simpleLogin js
 * @returns {undefined}
 */
(function () {
  "use strict";
  (function (angular) {
    // Declare app level module which depends on filters, and services
    angular.module('cm-ionic.config', [])

      // version of this seed app is compatible with angularFire 0.8.2
      // see tags for other versions: https://github.com/firebase/angularFire-seed/tags
      .constant('version', '0.1.0')

      // where to redirect users if they need to authenticate (see routeSecurity.js)
//    .constant('loginRedirectPath', '/tab/login')
      .constant('loginStateName', 'login')
      .constant('alreadyLoggedInStateName', 'tab.account')

      // your Firebase data URL goes here, no trailing slash
      .constant('FBURL', 'https://lobotime.firebaseio.com');

  })(angular);

})();

