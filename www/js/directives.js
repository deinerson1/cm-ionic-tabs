/* Directives */

(function () {
  "use strict";
  (function (angular) {

    angular.module('cm-ionic.directives', ['simpleLogin'])

      .directive('appVersion', function (version) {
        return function (scope, elm) {
          elm.text(version);
        };
      })

      /**
       * A directive that shows elements only when user is logged in.
       */
      .directive('ngShowAuth', function (simpleLogin, $timeout) {
        var isLoggedIn;
        simpleLogin.watch(function (user) {
          isLoggedIn = !!user;
        });

        return {
          restrict : 'A',
          link : function (scope, el) {
            el.addClass('ng-cloak'); // hide until we process it

            function update() {
              // sometimes if ngCloak exists on same element, they argue, so make sure that
              // this one always runs last for reliability
              $timeout(function () {
                el.toggleClass('ng-cloak', !isLoggedIn);
              }, 0);
            }

            update();
            simpleLogin.watch(update, scope);
          }
        };
      })

      /**
       * A directive that shows elements only when user is logged out.
       */
      .directive('ngHideAuth', function (simpleLogin, $timeout) {
        var isLoggedIn;
        simpleLogin.watch(function (user) {
          isLoggedIn = !!user;
        });

        return {
          restrict : 'A',
          link : function (scope, el) {
            function update() {
              el.addClass('ng-cloak'); // hide until we process it

              // sometimes if ngCloak exists on same element, they argue, so make sure that
              // this one always runs last for reliability
              $timeout(function () {
                el.toggleClass('ng-cloak', isLoggedIn !== false);
              }, 0);
            }

            update();
            simpleLogin.watch(update, scope);
          }
        };
      })

      .directive('autoComplete', ['$http', function ($http) {
          return {
            restrict : 'AE',
            scope : {
              selectedTags : '=model'
            },
            templateUrl : '/templates/facility-autocomplete.html',
            link : function (scope, elem, attrs) {

              scope.suggestions = [];

              scope.selectedTags = [];

              scope.selectedIndex = -1;

              scope.removeTag = function (index) {
                scope.selectedTags.splice(index, 1);
              };

              scope.search = function () {
                $http.get(attrs.url + '?unmBuildingName=' + scope.searchText).success(function (data) {
                  if (data.indexOf(scope.searchText) === -1) {
                    data.unshift(scope.searchText);
                  }
                  scope.suggestions = data;
                  scope.selectedIndex = -1;
                });
              };

              scope.addToSelectedTags = function (index) {
                if (scope.selectedTags.indexOf(scope.suggestions[index]) === -1) {
                  scope.selectedTags.push(scope.suggestions[index]);
                  scope.searchText = '';
                  scope.suggestions = [];
                }
              };

              scope.checkKeyDown = function (event) {
                if (event.keyCode === 40) {
                  event.preventDefault();
                  if (scope.selectedIndex + 1 !== scope.suggestions.length) {
                    scope.selectedIndex++;
                  }
                }
                else if (event.keyCode === 38) {
                  event.preventDefault();
                  if (scope.selectedIndex - 1 !== -1) {
                    scope.selectedIndex--;
                  }
                }
                else if (event.keyCode === 13) {
                  scope.addToSelectedTags(scope.selectedIndex);
                }
              };

              scope.$watch('selectedIndex', function (val) {
                if (val !== -1) {
                  scope.searchText = scope.suggestions[scope.selectedIndex];
                }
              });
            }
          };
        }]);

  })(angular);

})();