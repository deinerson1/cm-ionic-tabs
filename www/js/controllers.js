(function () {
  "use strict";

  angular.module('cm-ionic.controllers', [])

    // A simple controller that fetches a list of data from a service
    .controller('ClockIndexCtrl', function ($scope, ClockService, $ionicLoading, $log, $state) {
      $scope.auth = {};
      $scope.clocks = ClockService.all();

      $scope.data = {
        showDelete : false
      };

      $scope.onItemDelete = function (item) {
        // TODO make a deep (by value) copy of item.
        // store it in an undo "table"?

        $log.debug("$firebase $index Removed: " + item.$id);
        $log.debug("Lobotime Device ID Removed: " + item.lobotimeDeviceId);
        //$scope.clocks.splice($scope.clocks.indexOf(item), 1);
        //$scope.clocks.$remove(item.$id);
        //console.log("Device ID Removed: " + item.id);
      };

      // Trigger the loading indicator
      // TODO: Can this be used to display login messages? Yes. Where does a $watch fit in.
      // Create a watch on an error object.
      $scope.show = function () {

        // Show the loading overlay and text
        $scope.loading = $ionicLoading.show({
          // The text to display in the loading indicator
          content : 'Loading',
          // The animation to use
          animation : 'fade-in',
          // Will a dark overlay or backdrop cover the entire view
          showBackdrop : true,
          // The maximum width of the loading indicator
          // Text will be wrapped if longer than maxWidth
          maxWidth : 200,
          // The delay in showing the indicator
          showDelay : 500
        });
      };

      // Hide the loading indicator
      $scope.hide = function () {
        $scope.loading.hide();
      };

    })


// A simple controller that shows a tapped item's data
    .controller('ClockDetailCtrl', function ($scope, $stateParams, ClockService, $firebase, $http, $templateCache, $log) {
      // This demonstrates 3-way data binding to a firebase factory service
      ClockService.get($stateParams.clockId).$bindTo($scope, 'clock')
        .then(function () {
          //$scope.clock.$priority = $stateParams.clockId;
          $scope.clock.lastUpdated = Firebase.ServerValue.TIMESTAMP;
        });

      // read facilities.json
      $scope.facilities = [];

      $scope.statusList = ['new', 'network issue', 'New IP address needed', 'NAT IP address', 'installed', 'needs power', 'On Hold - Kronos Ticket', 'On Hold - Help.UNM Ticket', 'spare', 'on order from Kronos', 'purchase request in process', 'return to Kronos', 'device manager (WDM) issue', 'server-side issue', 'needs clock visit by technician', 'soft key issue', 'N/A', 'unknown'];

      $scope.patchPanelList = ['Panel A', 'Panel B', 'Panel C', 'Panel D', 'unknown', 'N/A'];
      $scope.rackList = ['Rack 1', 'Rack 2', 'Rack 3', 'Rack 4', 'unknown', 'N/A'];
      $scope.assetTagList = ['Tag 1', 'Tag 2', 'Tag 3', 'Tag 4', 'unknown', 'N/A'];

      $http({method : 'GET', url : 'data/facilities-2014-10-16.json', cache : $templateCache}).
        success(function (data) {
          $scope.facilities = data;
          $scope.selectedFacility = $scope.facilities.unmBuildingLabel;
          $log.debug('ClockDetailCtrl succeeded in reading and parsing facilities.json.');
        }).
        error(function (data, status, headers, config) {
          $log.error('EditCtrl failed to read or parse facilities.json.');
          $log.error(status);
          $log.error(headers);
          $log.error(config);
        });
    })

    .controller('addClockCtrl', function ($scope, $log) {

//    var clockCount = $scope.clocks.length;
//  $log.debug('Count: ' + clockCount);

      $scope.addClock = function (newClock) {
        // AngularFire $add method
//        $scope.clocks.$add({
//        id : 5,
//        lobotimeDeviceId : newClock.lobotimeDeviceId,
//        newClockStatus : newClock.newClockStatus,
//        lastUpdated : new Date(),
//        created : new Date()
//      });
        $scope.clocks.$add(newClock);
        //or add a new person manually
        //clockRef.update({name : 'Alex', age : 35});

        //$scope.newClock = "";
        // TODO redirect to tab.clock-detail
        //$location.path('/clocks/' + newClock.id);

      };
    })

    .controller('LoginCtrl', function ($scope, simpleLogin, $state, $log) {
      //$log.debug('LoginCtrl fired.');

      $scope.email = null;
      $scope.pass = null;

      $scope.login = function (email, pass) {
        $scope.err = null;
        //$log.debug('$scope.login function fired.', email, pass);
        simpleLogin.login(email, pass)
          .then(function (/* user */) {
            //$scope.user= user;
            //$log.debug('Logged in.');
            $state.go('tab.clock-index');
          }, function (err) {
            $scope.err = errMessage(err);
            $log.error(errMessage(err));
          });
      };

      function errMessage(err) {
        return angular.isObject(err) && err.code ? err.code : err + '';
      }
    })

    .controller('AccountCtrl', function ($scope, simpleLogin, fbutil, $log, user) {
      $log.debug('AccountCtrl fired.');
      $log.debug('user', user.uid);

      // create a 3-way binding with the user profile object in Firebase
      var profile = fbutil.syncObject(['users', user.uid]);
      profile.$bindTo($scope, 'profile');
      // expose logout function to scope
      $scope.logout = function () {
        profile.$destroy();
        simpleLogin.logout();
      };

      $scope.changePassword = function (pass, confirm, newPass) {
        resetMessages();
        if (!pass || !confirm || !newPass) {
          $scope.err = 'Please fill in all password fields';
        }
        else if (newPass !== confirm) {
          $scope.err = 'New pass and confirm do not match';
        }
        else {
          simpleLogin.changePassword(profile.email, pass, newPass)
            .then(function () {
              $scope.msg = 'Password changed';
            }, function (err) {
              $scope.err = err;
            });
        }
      };

      $scope.clear = resetMessages;

      $scope.changeEmail = function (pass, newEmail) {
        resetMessages();
        profile.$destroy();
        simpleLogin.changeEmail(pass, newEmail)
          .then(function (user) {
            profile = fbutil.syncObject(['users', user.uid]);
            profile.$bindTo($scope, 'profile');
            $scope.emailmsg = 'Email changed';
          }, function (err) {
            $scope.emailerr = err;
          });
      };

      function resetMessages() {
        $scope.err = null;
        $scope.msg = null;
        $scope.emailerr = null;
        $scope.emailmsg = null;
      }

    });

})();